-- create database summativedb
CREATE DATABASE summativedb;

-- using database summativedb
USE summativedb;

-- create table student
CREATE TABLE students(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    surname VARCHAR(50) NOT NULL,
    birthdate DATE NOT NULL,
    gender VARCHAR(6) NOT NULL
);

-- create table lesson
CREATE TABLE lessons(
	id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    level VARCHAR(50) NOT NULL
);

-- create table lesson
CREATE TABLE scores(
	id INT AUTO_INCREMENT PRIMARY KEY,
    student_id INT NOT NULL,
    lesson_id INT NOT NULL,
	score INT NULL,
    KEY student_id (`student_id`),
	CONSTRAINT student_id FOREIGN KEY (`student_id`) REFERENCES students (`id`),
    KEY lesson_id (`lesson_id`),
	CONSTRAINT lesson_id FOREIGN KEY (`lesson_id`) REFERENCES lessons (`id`)
);

-- =====================================

-- insert record for table students
INSERT INTO students (name, surname, birthdate, gender) 
	VALUES 
		('Harun', 'Al Rosyid', '1996-05-22','Male'),
        ('Romi', 'Robiyan', '1996-06-23','Male'),
        ('Ahmad', 'Sofyan', '1996-07-24','Male'),
        ('Rani', 'Lestari', '1996-08-25','Female'),
        ('Novia', 'Novianti', '1996-09-26','Female');
        
-- view record for table students        
SELECT id, name, surname, birthdate, gender
	FROM students;
    
-- insert record for table lessons
INSERT INTO lessons (name, level) 
	VALUES 
		('DATABASES SQL', 'Beginner'),
        ('JAVA Intermediate', 'Intermediate'),
        ('JAVA Advance', 'Expert');
        
-- view record for table lessons        
SELECT id, name, level
	FROM lessons;

-- insert record for table lessons
INSERT INTO scores (student_id, lesson_id, score)
	VALUES
		(1, 1, 80),
        (1, 2, 70),
        (1, 3, 80),
        (2, 1, 85),
        (2, 2, 75),
        (2, 3, 80),
        (3, 1, 95),
        (3, 2, 75),
        (3, 3, 85),
        (4, 1, 95),
        (4, 2, 85),
        (4, 3, 65),
        (5, 1, 55),
        (5, 2, 75),
        (5, 3, 85);

-- view record for table lessons        
SELECT id, student_id, lesson_id, score
	FROM scores;
        