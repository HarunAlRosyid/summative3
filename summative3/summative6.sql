
-- using database summativedb
USE summativedb;

-- create table department
CREATE TABLE department (
	department_id INT NOT NULL PRIMARY KEY, 
    department_name VARCHAR(30) NOT NULL, 
    manager_id INT NOT NULL,
	location_id INT NOT NULL
);

-- add foreign key tables employee 
ALTER TABLE employee 
	ADD FOREIGN KEY (department_id) REFERENCES department(department_id);


-- insert tables department
INSERT INTO department 
	(department_id, department_name, manager_id, location_id)
	VALUES 
		(10, 'Administration', 200, 1700),
		(20, 'Marketing', 201, 1800),
		(30, 'Purchasing', 114, 1700),
		(40, 'Human Resources', 203, 2400),
		(50, 'Shipping', 121, 1500),
		(60, 'IT', 103, 1400),
		(70, 'Public Relations', 204, 2700),
		(80, 'Sales', 145, 2500),
		(90, 'Executive', 100, 1700),
		(100, 'Finance', 108, 1700),
		(110, 'Accounting', 205, 1700),
		(120, 'Treasury', 0, 1700),
		(130, 'Corporate Tax', 0, 1700),
		(140, 'Control And Credit ', 0, 1700),
		(150, 'Shareholder Services', 0, 1700),
		(160, 'Benefits', 0, 1700),
		(170, 'Manufacturing', 0, 1700),
		(180, 'Construction ', 0, 1700),
		(190, 'Contracting', 0, 1700),
		(200, 'Operations', 0, 1700),
		(210, 'IT Support', 0, 1700),
		(220, 'NOC', 0, 1700),
		(230, 'IT Helpdesk', 0, 1700),
		(240, 'Government Sales', 0, 1700),
		(250, 'Retail Sales ', 0, 1700),
		(260, 'Recruiting', 0, 1700),
		(270, 'Payroll', 0, 1700);

-- view record for table department 
SELECT department_id, department_name, manager_id, location_id
	FROM department;