-- using database summativedb
USE summativedb;

-- view with joins tables students,lessons and scores
SELECT lessons.id as lessons_id, students.id as student_id, scores.id as score_id, 
	   lessons.name as lesson_name, lessons.level, 
       students.name as student_name, students.surname, students.birthdate, students.gender,
	   scores.score
	FROM scores 
		LEFT JOIN students
			ON students.id = scores.student_id
		LEFT JOIN lessons
			ON lessons.id = scores.lesson_id
		ORDER BY lessons.name ASC
