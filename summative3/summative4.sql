-- view record for table employee by hire date Augustus
SELECT employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commision_pct, manager_id, department_id
	FROM employee
		WHERE hire_date
			BETWEEN '1987-08-01' AND '1987-08-31' 
	ORDER BY hire_date ASC;