-- using database summativedb
USE summativedb;

-- select departement ID,  departement name, year, count employee joined.
SELECT department.department_id, department.department_name, substr(employee.hire_date,1,4) as year, 
	COUNT(employee.employee_id) AS total_employee
	FROM department 
		RIGHT JOIN employee
			ON department.department_id = employee.department_id
	GROUP BY department.department_name
	ORDER BY department.department_name ASC;
	
	